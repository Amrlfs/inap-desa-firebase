import 'package:inapdesa_firebase/UI/B4_Booking/Booking.dart';
import 'package:flutter/material.dart';
import 'package:inapdesa_firebase/UI/B1_Home/B1_Home_Screen/B1_Home_Screen.dart';
import 'package:inapdesa_firebase/UI/B2_Location/B2_LocationScreen.dart';
import 'package:inapdesa_firebase/UI/B3_Trips/B3_TripScreen.dart';
import 'package:inapdesa_firebase/UI/B5_Profile/B5_ProfileScreen.dart';
import 'custom_nav_bar.dart';

class bottomNavBar extends StatefulWidget {
  final String userID;
  bottomNavBar({this.userID});

  _bottomNavBarState createState() => _bottomNavBarState();
}

class _bottomNavBarState extends State<bottomNavBar> {
  int currentIndex = 0;
  Widget callPage(int current) {
    switch (current) {
      case 0:
        return new Home(
          userID: widget.userID,
        );
        break;
      case 1:
        return new noLocation(
          userID: widget.userID,
        );
        break;
      case 2:
        return new trip(
          userID: widget.userID,
        );
        break;
      case 3:
        return new BookingScreen(
          idUser: widget.userID,
        );
        break;
      case 4:
        return new profile(
          userID: widget.userID,
        );
        break;
      default:
        return new Home(
          userID: widget.userID,
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: callPage(currentIndex),
      bottomNavigationBar: BottomNavigationDotBar(
          color: Colors.black26,
          items: <BottomNavigationDotBarItem>[
            BottomNavigationDotBarItem(
                icon: const IconData(0xf7f5, fontFamily: 'MaterialIcons'),
                onTap: () {
                  setState(() {
                    currentIndex = 0;
                  });
                }),
            BottomNavigationDotBarItem(
                icon:const IconData(0xe3ac, fontFamily: 'MaterialIcons'),
                onTap: () {
                  setState(() {
                    currentIndex = 1;
                  });
                }),
            BottomNavigationDotBarItem(
                icon: const IconData(
                  0xe67c,
                  fontFamily: 'MaterialIcons',
                ),
                onTap: () {
                  setState(() {
                    currentIndex = 2;
                  });
                }),
            BottomNavigationDotBarItem(
                icon: const IconData(
                  0xe0f3,
                  fontFamily: 'MaterialIcons',
                ),
                onTap: () {
                  setState(() {
                    currentIndex = 3;
                  });
                }),
            BottomNavigationDotBarItem(
                icon: const IconData(0xe043, fontFamily: 'MaterialIcons'),
                onTap: () {
                  setState(() {
                    currentIndex = 4;
                  });
                }),
          ]),
    );
  }
}
