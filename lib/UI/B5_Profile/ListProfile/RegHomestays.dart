// ignore_for_file: deprecated_member_use

import 'package:inapdesa_firebase/Library/Multiple_Language/Language_Library/lib/easy_localization_delegate.dart';
import 'package:inapdesa_firebase/Library/Multiple_Language/Language_Library/lib/easy_localization_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class regHomestays extends StatefulWidget {
  @override
  _regHomestaysState createState() => _regHomestaysState();
}

class _regHomestaysState extends State<regHomestays> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  String _name, _ic, _email, _phone, _homeName, _address, _location;
  String _status = 'Not Verify';
  TextEditingController name = new TextEditingController();
  TextEditingController ic = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController phone = new TextEditingController();
  TextEditingController homeName = new TextEditingController();
  TextEditingController address = new TextEditingController();
  TextEditingController location = new TextEditingController();

  void addData() {
    FirebaseFirestore.instance.runTransaction((Transaction transaction) async {
      FirebaseFirestore.instance.collection("registration").add({
        "Name": _name,
        "IC Number" : _ic,
        "Email" : _email,
        "Phone" : _phone,
        "Homestay Name" : _homeName,
        "Address" : _address,
        "Location" : _location,
        "Status" : _status,

      });
    });
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0.0,
          title: Text(AppLocalizations.of(context).tr('regHomestay'),
              style: TextStyle(fontFamily: "Sofia", color: Colors.black)),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.only(right: 12.0, left: 12.0, top: 15.0),
          child: ListView(
            children: <Widget>[
              Text(
                AppLocalizations.of(context).tr('regHomestay'),
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Color(0xFF0079F0),
                    border: Border.all(width: 1.2, color: Color(0xFF0079F0))),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Text(
                    AppLocalizations.of(context).tr('regHomestaysDesc'),
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        letterSpacing: 1.2,
                        fontFamily: "Sans",color: Colors.white),
                  ),
                ),
              ),
              Form(
                key: _form,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      AppLocalizations.of(context).tr('name1'),
                      style: TextStyle(
                          fontWeight: FontWeight.w500, fontSize: 15.0),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      // ignore: missing_return
                      validator: (input) {
                        if (input.isEmpty) {
                          return AppLocalizations.of(context).tr('pleaseName');
                        }
                      },
                      onSaved: (input) => _name = input,
                      controller: name,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                          fontFamily: "WorkSansSemiBold",
                          fontSize: 16.0,
                          color: Colors.black),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 0.5,
                                color: Colors.black12.withOpacity(0.01))),
                        contentPadding: EdgeInsets.all(13.0),
                        hintText: AppLocalizations.of(context).tr('inputName'),
                        hintStyle:
                        TextStyle(fontFamily: "Sans", fontSize: 15.0),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(AppLocalizations.of(context).tr('email'),
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15.0)),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      // ignore: missing_return
                      validator: (input) {
                        if (input.isEmpty) {
                          return AppLocalizations.of(context).tr('pleaseEmail');
                        }
                      },
                      onSaved: (input) => _email = input,
                      controller: email,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                          fontFamily: "WorkSansSemiBold",
                          fontSize: 16.0,
                          color: Colors.black),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 0.5,
                                color: Colors.black12.withOpacity(0.01))),
                        contentPadding: EdgeInsets.all(13.0),
                        hintText: AppLocalizations.of(context).tr('inputEmail'),
                        hintStyle:
                        TextStyle(fontFamily: "Sans", fontSize: 15.0),
                      ),
                    ),

                    SizedBox(
                      height: 20.0,
                    ),
                    Text(AppLocalizations.of(context).tr('icNumber'),
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15.0)),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      // ignore: missing_return
                      validator: (input) {
                        if (input.isEmpty) {
                          return AppLocalizations.of(context).tr('pleaseIC');
                        }
                      },
                      onSaved: (input) => _ic = input,
                      controller: ic,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                          fontFamily: "WorkSansSemiBold",
                          fontSize: 16.0,
                          color: Colors.black),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 0.5,
                                color: Colors.black12.withOpacity(0.01))),
                        contentPadding: EdgeInsets.all(13.0),
                        hintText: AppLocalizations.of(context).tr('inputIC'),
                        hintStyle:
                        TextStyle(fontFamily: "Sans", fontSize: 15.0),
                      ),
                    ),

                    SizedBox(
                      height: 20.0,
                    ),
                    Text(AppLocalizations.of(context).tr('phoneNumber'),
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15.0)),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      // ignore: missing_return
                      validator: (input) {
                        if (input.isEmpty) {
                          return AppLocalizations.of(context).tr('pleasePhone');
                        }
                      },
                      onSaved: (input) => _phone = input,
                      controller: phone,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                          fontFamily: "WorkSansSemiBold",
                          fontSize: 16.0,
                          color: Colors.black),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 0.5,
                                color: Colors.black12.withOpacity(0.01))),
                        contentPadding: EdgeInsets.all(13.0),
                        hintText: AppLocalizations.of(context).tr('inputPhone'),
                        hintStyle:
                        TextStyle(fontFamily: "Sans", fontSize: 15.0),
                      ),
                    ),

                    SizedBox(
                      height: 20.0,
                    ),
                    Text(AppLocalizations.of(context).tr('homeName'),
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15.0)),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      // ignore: missing_return
                      validator: (input) {
                        if (input.isEmpty) {
                          return AppLocalizations.of(context).tr('pleaseHome');
                        }
                      },
                      onSaved: (input) => _homeName = input,
                      controller: homeName,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                          fontFamily: "WorkSansSemiBold",
                          fontSize: 16.0,
                          color: Colors.black),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 0.5,
                                color: Colors.black12.withOpacity(0.01))),
                        contentPadding: EdgeInsets.all(13.0),
                        hintText: AppLocalizations.of(context).tr('inputHome'),
                        hintStyle:
                        TextStyle(fontFamily: "Sans", fontSize: 15.0),
                      ),
                    ),

                    SizedBox(
                      height: 20.0,
                    ),
                    Text(AppLocalizations.of(context).tr('locationName'),
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15.0)),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      // ignore: missing_return
                      validator: (input) {
                        if (input.isEmpty) {
                          return AppLocalizations.of(context).tr('pleaseLocation');
                        }
                      },
                      onSaved: (input) => _location = input,
                      controller: location,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                          fontFamily: "WorkSansSemiBold",
                          fontSize: 16.0,
                          color: Colors.black),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 0.5,
                                color: Colors.black12.withOpacity(0.01))),
                        contentPadding: EdgeInsets.all(13.0),
                        hintText: AppLocalizations.of(context).tr('inputLocation'),
                        hintStyle:
                        TextStyle(fontFamily: "Sans", fontSize: 15.0),
                      ),
                    ),

                    SizedBox(
                      height: 20.0,
                    ),
                    Text(AppLocalizations.of(context).tr('detailAddress'),
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15.0)),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      color: Colors.white,
                      width: double.infinity,
                      child: TextFormField(
                        // ignore: missing_return
                        validator: (input) {
                          if (input.isEmpty) {
                            return AppLocalizations.of(context)
                                .tr('pleaseAddress');
                          }
                        },
                        maxLines: 10,
                        onSaved: (input) => _address = input,
                        controller: address,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.words,
                        style: TextStyle(
                            fontFamily: "WorkSansSemiBold",
                            fontSize: 16.0,
                            color: Colors.black),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 0.5,
                                  color: Colors.black12.withOpacity(0.01))),
                          contentPadding: EdgeInsets.all(13.0),
                          hintText:
                          AppLocalizations.of(context).tr('inputAddress'),
                          hintStyle:
                          TextStyle(fontFamily: "Sans", fontSize: 15.0),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    InkWell(
                      onTap: () {
                        final formState = _form.currentState;

                        if (formState.validate()) {
                          formState.save();
                          setState(() {});

                          addData();
                          _showDialog(context);
                        } else {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text(
                                      AppLocalizations.of(context).tr('error')),
                                  content: Text(AppLocalizations.of(context)
                                      .tr('pleaseInformation')),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text(AppLocalizations.of(context)
                                          .tr('close')),
                                      onPressed: () {
                                        Navigator.of(context).pop();

                                      },
                                    )
                                  ],
                                );
                              });
                        }
                      },
                      child: Container(
                        height: 50.0,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Color(0xFF0079F0),
                            borderRadius:
                            BorderRadius.all(Radius.circular(40.0))),
                        child: Center(
                          child: Text(
                            AppLocalizations.of(context).tr('inputData'),
                            style: TextStyle(
                                fontFamily: "Popins",
                                color: Colors.white,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/// Card Popup if success payment
_showDialog(BuildContext ctx) {
  showDialog(
    context: ctx,
    barrierDismissible: true,
    builder: (BuildContext context) => SimpleDialog(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: InkWell(
                    onTap: () {
                      Navigator.of(ctx).pop();
                    },
                    child: Icon(
                      Icons.close,
                      size: 30.0,
                    ))),
            SizedBox(
              width: 10.0,
            )
          ],
        ),
        Container(
            padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
            color: Colors.white,
            child: Icon(
              Icons.check_circle,
              size: 150.0,
              color: Colors.blueAccent,
            )),
        Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0),

              child: Text(
                AppLocalizations.of(context).tr('success'),
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 22.0),
                textAlign: TextAlign.center,

              ),
            )),
        Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
              child: Text(
                AppLocalizations.of(context).tr('regHomeSuccess'),
                style: TextStyle(fontSize: 17.0),
              ),
            )),
      ],
    ),
  );
}
