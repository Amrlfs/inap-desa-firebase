import 'package:google_maps_flutter/google_maps_flutter.dart';

class Homestay {
  String shopName;
  String address;
  String description;
  String thumbNail;
  LatLng locationCoords;

  Homestay(
      {this.shopName,
      this.address,
      this.description,
      this.thumbNail,
      this.locationCoords});
}

final List<Homestay> homestayLocation = [
  Homestay(
      shopName: 'Inap Desa Kak Limah',
      address: '5b, Jalan Raja Mahmud, Kampung Baru, 50300 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur',
      description:
          'Its centrally located on a quiet street with reasonable price. 15 minutes by taxi to LRT Kampung Baru. Only 30 minutes’ walk to Kuala Lumpur City.',
      locationCoords: LatLng(3.164385, 101.704580),
      thumbNail: 'https://cktravels.com/wp-content/uploads/2019/02/IMG_3086.jpg'
      ),
  Homestay(
      shopName: 'Inap Desa Abu',
      address: '30, Jalan Keramat, Kampung Datuk Keramat, 54000 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur',
      description:
          'Its centrally located on a quiet street with reasonable price. Only 5 minutes’ walk to  LRT Damai.',
      locationCoords: LatLng(3.1676801, 101.7283806),
      thumbNail: 'https://media.karousell.com/media/photos/products/2021/5/5/rumah_2_tingkat_taman_sri_gomb_1620197607_a0d98f6b_progressive'
      ),

  Homestay(
      shopName: 'Inap Desa Kak Limah',
      address: 'Seksyen 4, Lamping Bukit Kulat, Jalan Pantai Tengah, 07000 Langkawi, Kedah',
      description:
      'Its centrally located on a quiet street with side of Genang Hill beach..',
      locationCoords: LatLng(6.277004, 99.731264),
      thumbNail: 'https://pix10.agoda.net/hotelImages/agoda-homes/6748752/d550cd948ad335e1add8d621889f79a6.jpg?ca=8&ce=1&s=1024x768'
  ),
  Homestay(
      shopName: 'Inap Desa Melur',
      address: 'Jalan Makam Mahsuri, 07000, Pulau Langkawi., Kedah, 07000 Pulau Langkawi.',
      description:
      'Its centrally located on a quiet street with side of Gunung Raya and SK Seri Lagenda.',
      locationCoords: LatLng(6.378086, 99.789173),
      thumbNail: 'https://q-xx.bstatic.com/xdata/images/hotel/840x460/86500091.jpg?k=da0cdb737f45a5562c64bc5b281d0e4f63686c281ddf402d168bb092bde33ea3&o='
  ),

  Homestay(
      shopName: 'Inap Desa Pak Lah',
      address: 'Brinchang, 39000 Brinchang, Pahang',
      description:
      'Its centrally located on a quiet street with side of Strawberry farm.',
      locationCoords: LatLng(4.503153, 101.407574),
      thumbNail: 'https://pbs.twimg.com/media/EeVvzD4UEAAXv6E.jpg:large'
  ),

  Homestay(
      shopName: 'Inap Desa Evergreen',
      address: 'Brinchang, 39000 Brinchang, Pahang',
      description:
      'Its centrally located on a quiet street with side of Tea farm.',
      locationCoords: LatLng(4.522168, 101.393669),
      thumbNail: 'https://4.bp.blogspot.com/-Rk3FenGhEms/W4NfwYMptOI/AAAAAAAAe3c/wdjgSYmAQMoyfMyKX6eY7mejsg0RCRzgwCLcBGAs/s1600/_MG_3086.JPG'
  ),

  Homestay(
      shopName: 'Inap Desa Tanjung Gemok',
      address: 'Pantai Tanjung Gemok, 71000 Port Dickson, Negeri Sembilan',
      description:
      'Its centrally located on a quiet street with side of beach.',
      locationCoords: LatLng(2.544702, 101.799317),
      thumbNail: 'https://kakitravel.net/wp-content/uploads/2019/08/20-1-1024x683.jpg'
  ),

  Homestay(
      shopName: 'Inap Desa Sri Ledang',
      address: 'Lot 387, Kampung Si Rusa, 71050 Port Dickson, Negeri Sembilan',
      description:
      'Its centrally located on a quiet street with side of villages.',
      locationCoords: LatLng(2.500838, 101.842672),
      thumbNail: 'https://pix10.agoda.net/hotelImages/21824348/0/8df5ca5bdd262343c2b1de8b93e62355.jpg?ca=16&ce=1&s=1024x768'
  ),

  Homestay(
      shopName: 'Inap Desa Pandan',
      address: 'Lorong Sekolah, Seri Bukit Batu, 84000 Muar, Johor',
      description:
      'Its centrally located on a quiet street with side of villages.',
      locationCoords: LatLng(2.068001, 102.630550),
      thumbNail: 'http://grandpandan.weebly.com/uploads/8/4/4/3/8443409/header_images/1397982763.jpg'
  ),

  Homestay(
      shopName: 'Inap Desa Tepi Sungai',
      address: '32, Jalan Teluk Sisek, 25000 Kuantan, Pahang',
      description:
      'Its centrally located on a quiet street with side of Kuantan river and city',
      locationCoords: LatLng(3.806971, 103.330398),
      thumbNail: 'https://pix10.agoda.net/hotelImages/agoda-homes/18007136/d88d8d7be8832c03720a6e3a1c87901b.jpg?ca=13&ce=1&s=1024x768'
  ),


  //Discover
  Homestay(
      shopName: 'Inap Desa Gurney',
      address: 'Jalan Bukit Keramat, Kampung Datuk Keramat, 54000 Kuala Lumpur, Federal Territory of Kuala Lumpur',
      description:
      'Its centrally located on a quiet street with side of Kuala Lumpur City',
      locationCoords: LatLng(3.169871, 101.727093),
      thumbNail: 'https://pix10.agoda.net/hotelImages/5907810/0/8d4b77fd35af1efd816e42e9c72c53f2.jpg?ca=14&ce=1&s=768x1024'
  ),

  Homestay(
      shopName: 'Inap Desa Syed',
      address: '194, Jalan Mawar 4, Taman Bunga Mawar, 84000 Muar, Johor',
      description:
      'Its centrally located on a quiet street with side of Muar City',
      locationCoords: LatLng(2.031600, 102.560899),
      thumbNail: 'https://muarhomestay.files.wordpress.com/2013/03/dari-hadapan-rumah.jpg'
  ),

  Homestay(
      shopName: 'Inap Desa Makmur',
      address: '25, Lrg Wawasan Indah 1A, Kampung Padang Gaong, 07000 Langkawi, Kedah',
      description:
      'Its centrally located on a quiet street with side of LangkawiCity',
      locationCoords: LatLng(6.350202, 99.762287),
      thumbNail: 'https://cf.bstatic.com/xdata/images/hotel/max1280x900/186766257.jpg?k=e0066e75be9847bff75d7890f4a00898f74ebe1a85327255d09497f31f25413a&o=&hp=1'
  ),

  Homestay(
      shopName: 'Inap Desa Walid',
      address: 'Batu 31, Site, 39200, Habu, 39000 Ringlet, Pahang',
      description:
      'Its centrally located on a quiet street with side of Habu Height',
      locationCoords: LatLng(4.434722, 101.390087),
      thumbNail: 'https://pix10.agoda.net/hotelImages/445/4453892/4453892_18020722060061618083.jpg?ca=6&ce=1&s=1024x768'
  ),

  Homestay(
      shopName: 'Inap Desa Paklong',
      address: '111, Lorong Vista Jaya 4, 71010 Port Dickson, Negeri Sembilan',
      description:
      'Its centrally located on a quiet street with side of SPDH Highway',
      locationCoords: LatLng(2.574646, 101.859195),
      thumbNail: 'https://www.andyrahmanarchitect.com/projects/images/Various/Rumah%20Kos%20Keputih%20Jilid%202/1.%20Eksterior%20View%20Siang.jpg'
  ),

  Homestay(
      shopName: 'Inap Desa Minah',
      address: '47, Lorong IM 2/88, Bandar Indera Mahkota, 25200 Kuantan, Pahang',
      description:
      'Its centrally located on a quiet street with side of Surau-Al Hidayah',
      locationCoords: LatLng(3.821373, 103.300070),
      thumbNail: 'https://cf.bstatic.com/xdata/images/hotel/max1024x768/278191523.jpg?k=a1706f29343d8c2195cd68989bab47d23d8827d6f03d8f5d73c694b2bca1cb07&o=&hp=1'
  ),








];